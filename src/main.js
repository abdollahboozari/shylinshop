import Vue from 'vue'
import App from './App.vue'
import VueCookie from 'vue-cookie'
import Vuelidate from 'vuelidate'
import VueResource from 'vue-resource'
import store from './store'
import router from './router'
import vOutsideEvents from 'vue-outside-events'
// import VueAxios from 'vue-axios'
// import axios from 'axios'
// require('../public/static/css/style.css');

Vue.use(VueResource);
Vue.use(Vuelidate);
// Vue.use(VueAxios, axios)
Vue.use(VueCookie);
Vue.use(vOutsideEvents);

Vue.http.options.root = "http://89.42.211.243:9000/v1/";

Vue.http.interceptors.push((request, next) => {
  if(Vue.cookie.get('accessToken') != null){
    request.headers.set('Authorization', 'Bearer ' + Vue.cookie.get('accessToken'));
  }
  next();
});

Vue.config.productionTip = false;

Vue.directive('click-outside2', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      // here I check that click was outside the el and his childrens
      if (!(el == event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
});

Vue.filter("custom-currency", (value) => {
  if(value == null) {
    return value;
  }else if (value < 1000) {
    return `${value}  تومان`;
  } else if (value < 1000000) {
    return `${value / 1000}  هزار تومان`;
  } else if (value < 1000000000) {
    return `${value / 1000000}  میلیون تومان`;
  } else if (value < 1000000000000) {
    return `${value / 1000000000000}  میلیارد تومان`;
  }
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
