import Vue from 'vue'
import Vuex from 'vuex'
import User from './modules/User'
import Cart from "./modules/Cart"
import Product from "./modules/Product"
import Banner from "./modules/Banner"
import Post from "./modules/Post"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
    },
    mutations: {
    },
    actions: {
    },
    modules: {
        User,
        Cart,
        Product,
        Banner,
        Post
    }
})
