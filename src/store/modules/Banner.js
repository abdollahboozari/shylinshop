const state = {
    banners: [],
    bannersImageAddress: "/static/image/banner/",
};


const getters = {
    banners(state) {
      return state.banners;
    },
    bannersImageAddress(state) {
        return state.bannersImageAddress;
    },
};

const mutations = {
    setBanners(state, banners) {
      state.banners = banners;
    }
};

const actions = {
    getBannersAction(context) {
      let banners = [
        {id: 1, bannerImage: "banner.png"},
        {id: 2, bannerImage: "banner.png"},
        {id: 3, bannerImage: "banner.png"},
      ];
      context.commit("setBanners", banners);
    },
};

export default {
    state,
    getters,
    mutations,
    actions
};
