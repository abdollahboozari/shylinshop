// import Vue from 'vue';
// import productArray from "../productJson";

const state = {
  products: {},
  specialProducts: [],
  productImageAddress: "static/image/product/",
};

const getters = {
  productImageAddress(state) {
    return state.productImageAddress;
  },
  products(state) {
    return state.products;
  },
  specialProducts(state) {
    return state.specialProducts;
  },
};

const mutations = {
  setProducts(state, products) {
    state.products = products;
  },
  setSpecialProducts(state, products) {
    state.specialProducts = products;
  },
};

const actions = {
  getSpecialProductsAction(context) {
    let products = [
      {id:1 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 100000, discount: null, imagelink: "honey-larg.png"},
      {id:2 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 200000, discount: null, imagelink: "honey-larg.png"},
      {id:3 , title: "برنج درجه یک درود", description: null, unit: "کیلوگرم", weight: 1, price: 30000, discount: null, imagelink: "rice2.png"},
      {id:4 , title: "عسل درجه یک اشترانکوه", description: "موم دار", unit: "کیلوگرم", weight: 1, price: 80000, discount: 50000, imagelink: "honey-larg.png"},
      {id:5 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 100000, discount: null, imagelink: "honey-larg.png"},
      {id:6 , title: "برنج درجه یک درود", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 30000, discount: null, imagelink: "rice2.png"},
      {id:7 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 200000, discount: null, imagelink: "honey-larg.png"},
      {id:8 , title: "عسل درجه یک اشترانکوه", description: "موم دار", unit: "کیلوگرم", weight: 1, price: 80000, discount: 50000, imagelink: "honey-larg.png"},
    ];
    context.commit("setSpecialProducts", products);
  },
  getProductsAction(context, filter) {
    let productArray = [
      {id:1 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 100000, discount: null, imagelink: "honey-larg.png"},
      {id:2 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 200000, discount: null, imagelink: "honey-larg.png"},
      {id:3 , title: "برنج درجه یک درود", description: null, unit: "کیلوگرم", weight: 1, price: 30000, discount: null, imagelink: "rice2.png"},
      {id:4 , title: "عسل درجه یک اشترانکوه", description: "موم دار", unit: "کیلوگرم", weight: 1, price: 80000, discount: 50000, imagelink: "honey-larg.png"},
      {id:5 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 100000, discount: null, imagelink: "honey-larg.png"},
      {id:6 , title: "برنج درجه یک درود", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 30000, discount: null, imagelink: "rice2.png"},
      {id:7 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 200000, discount: null, imagelink: "honey-larg.png"},
      {id:8 , title: "عسل درجه یک اشترانکوه", description: "موم دار", unit: "کیلوگرم", weight: 1, price: 80000, discount: 50000, imagelink: "honey-larg.png"},
      {id:9 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 100000, discount: null, imagelink: "honey-larg.png"},
      {id:10 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 200000, discount: null, imagelink: "honey-larg.png"},
      {id:11 , title: "برنج درجه یک درود", description: null, unit: "کیلوگرم", weight: 1, price: 30000, discount: null, imagelink: "rice2.png"},
      {id:12 , title: "عسل درجه یک اشترانکوه", description: "موم دار", unit: "کیلوگرم", weight: 1, price: 80000, discount: 50000, imagelink: "honey-larg.png"},
    ];
    let productsObject = {
      pageCount: 40,
      pageSize: 12,
      currentPage: filter.pageId,
      products: productArray,
    };
    context.commit("setProducts", productsObject);
  },
};

export default {
  state,
  getters,
  mutations,
  actions
};
