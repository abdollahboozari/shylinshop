// import Vue from 'vue';

const state = {
  weblogPosts: [],
  cookingPosts: [],
};

const getters = {
  weblogPosts(state) {
    return state.weblogPosts;
  },
  cookingPosts(state) {
    return state.cookingPosts;
  },
};

const mutations = {
  setWeblogPosts(state, posts) {
    state.weblogPosts = posts;
  },
  setCookingPosts(state, posts) {
    state.cookingPosts = posts;
  },
};

const actions = {
  getWeblogPostsAction(context) {
    let posts = [
      {id:1 , title: "برخی فواید عسل", benefits:[{id: 1, title: "جلوگیری از ریفلاکس اسید معده"}, {id: 2, title: "درمانی برای سرفه بر اساس گزارش سازمان سلامت جهانی"}], tags: [{id: 1, title: "عسل"}, {id: 2, title: "فواید"}]},
      {id:2 , title: "برخی فواید عسل", benefits:[{id: 1, title: "جلوگیری از ریفلاکس اسید معده"}, {id: 2, title: "درمانی برای سرفه بر اساس گزارش سازمان سلامت جهانی"}], tags: [{id: 1, title: "عسل"}, {id: 2, title: "فواید"}]},
      {id:3 , title: "برخی فواید عسل", benefits:[{id: 1, title: "جلوگیری از ریفلاکس اسید معده"}, {id: 2, title: "درمانی برای سرفه بر اساس گزارش سازمان سلامت جهانی"}], tags: [{id: 1, title: "عسل"}, {id: 2, title: "فواید"}]},
      {id:4 , title: "برخی فواید عسل", benefits:[{id: 1, title: "جلوگیری از ریفلاکس اسید معده"}, {id: 2, title: "درمانی برای سرفه بر اساس گزارش سازمان سلامت جهانی"}], tags: [{id: 1, title: "عسل"}, {id: 2, title: "فواید"}]},
    ];
    context.commit("setWeblogPosts", posts);
  },
  getCookingPostsAction(context) {
    let posts = [
      {id:1 , title: "استفاده از عسل به جای شکر", description: "در رژیم غذایی خود میتوانید شکر را با عسل تازه جایگیزن کنید به طور مثال برای چای شیرین میتوانید مقداری عسل در چای حل کنید", tags: [{id: 1, title: "عسل"}, {id: 2, title: "کاربردها"}]},
      {id:2 , title: "استفاده از عسل به جای شکر", description: "در رژیم غذایی خود میتوانید شکر را با عسل تازه جایگیزن کنید به طور مثال برای چای شیرین میتوانید مقداری عسل در چای حل کنید", tags: [{id: 1, title: "عسل"}, {id: 2, title: "کاربردها"}]},
      {id:3 , title: "استفاده از عسل به جای شکر", description: "در رژیم غذایی خود میتوانید شکر را با عسل تازه جایگیزن کنید به طور مثال برای چای شیرین میتوانید مقداری عسل در چای حل کنید", tags: [{id: 1, title: "عسل"}, {id: 2, title: "کاربردها"}]},
      {id:4 , title: "استفاده از عسل به جای شکر", description: "در رژیم غذایی خود میتوانید شکر را با عسل تازه جایگیزن کنید به طور مثال برای چای شیرین میتوانید مقداری عسل در چای حل کنید", tags: [{id: 1, title: "عسل"}, {id: 2, title: "کاربردها"}]},
    ];
    context.commit("setCookingPosts", posts);
  },
};

export default {
  state,
  getters,
  mutations,
  actions
};
