import Vue from 'vue';
// import axios from 'axios'
import router from '../../router/index';
import Swal from 'sweetalert2'

const state = {
    isUserAuthenticated: false,
    usernameOrPasswordIsWrong: false,
};


const getters = {
    usernameOrPasswordIsWrong(state) {
        return state.usernameOrPasswordIsWrong;
    },
    isUserAuthenticated(state) {
        return state.isUserAuthenticated;
    }
};

const mutations = {
    setAuthCookie(state, token) {
        Vue.cookie.set("accessToken", token.access);
        Vue.cookie.set("refreshToken", token.refresh);
    },
    deleteAuthCookie() {
        // Vue.cookie.delete("authToken");
        Vue.cookie.delete("accessToken");
        Vue.cookie.delete("refreshToken");
    },
    setUserAuthenticated(state, isAuth) {
        state.isUserAuthenticated = isAuth;
    },
    setUsernameOrPasswordIsWrong(state, loginHasError) {
        state.usernameOrPasswordIsWrong = loginHasError;
    },
    setRegisterPhoneNumber(state, phone) {
        Vue.cookie.set(
            "phoneNumber",
            phone
        );
    },
    deleteRegisterPhoneNumber() {
        Vue.cookie.delete("phoneNumber");
    },
};

const actions = {
    registerAction(context, registerData) {
        Vue.http.post('users/register/', registerData)
            .then(response => {
                console.log(response);
                if (response.body.success) {
                    /*Swal.fire({
                        // position: 'top-end',
                        icon: 'success',
                        title: 'ثبت نام شما با موفقیت انجام شد.',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(() => { router.push('/login'); }, 3002);*/
                    context.commit("setRegisterPhoneNumber", registerData.username);
                    router.push('/activate');
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'اطلاعات وارد شده معتبر نمی باشد',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            }).catch((response) => {
                console.log(response);
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'خطایی سمت سرور رخ داده است',
                    showConfirmButton: false,
                    timer: 3000
                });
            });
    },
    activationAction(context, activateData) {
        console.log(activateData);
        Vue.http.post('users/verify-code/', activateData)
            .then(response => {
                console.log(response);
                if (response.body.success) {
                    context.commit("deleteRegisterPhoneNumber");
                    Swal.fire({
                        icon: 'success',
                        title: 'ثبت نام شما با موفقیت انجام شد.',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(() => { router.push('/login'); }, 3001);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'اطلاعات وارد شده معتبر نمی باشد',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            }).catch((response) => {
            console.log(response);
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'خطایی سمت سرور رخ داده است',
                showConfirmButton: false,
                timer: 3000
            });
        });
    },
    loginAction(context, data) {
        context.commit("deleteAuthCookie"); // چون توکن منقضی شده را ارسال می کند سمت سرور خطا می هد باید حذف شود.
        Vue.http.post('users/login/', data).then(response => {
            console.log(response);
            if (response.body.success) {
                let authToken = {
                    access: response.body.access,
                    refresh: response.body.refresh
                };
                context.commit("setAuthCookie", authToken);
                context.commit("setUserAuthenticated", true);
                router.push ('/');
            } else {
                context.commit("setUsernameOrPasswordIsWrong", true);
            }
        }).catch((response) => {
            console.log(response);
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'خطایی سمت سرور رخ داده است',
                showConfirmButton: false,
                timer: 3000
            });
        });
    },
    logOutAction(context) {
        let data = {
            refresh:  Vue.cookie.get('refreshToken')
        };
        console.log(data);
        Vue.http.post('users/logout/', data).then(response => {
            console.log(response);
            if (response.body.success) {
                context.commit("setUserAuthenticated", false);
                context.commit("deleteAuthCookie");
                router.push('/Login');
            }
        }).catch((response) => {
            console.log(response);
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'خطایی سمت سرور رخ داده است',
                showConfirmButton: false,
                timer: 3000
            });
        });
    },
    forgotPasswordAction(context, forgotData) {
        Vue.http.post('users/forgot-password/', forgotData)
            .then(response => {
                console.log(response);
                if (response.body.success) {
                    Swal.fire({
                        // position: 'top-end',
                        icon: 'success',
                        title: 'پسورد جدید به تلفن همراه ارسال شد.',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(() => { router.push('/login'); }, 3002);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'اطلاعات وارد شده معتبر نمی باشد',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            }).catch((response) => {
            console.log(response);
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'خطایی سمت سرور رخ داده است',
                showConfirmButton: false,
                timer: 3000
            });
        });
    },
    checkIsExistUserAction(data) {
        Vue.http.post('users/check-email-mobile/', data)
            .then(response => {
                console.log(response);
                return !!response.body.success;
            }).catch((response) => {
            console.log(response);
            return true;
        });
    },
    checkUserIsAuthenticatedAction(context) {
        Vue.http.get('users/test-login/').then(response => {
            console.log(response);
            if (response.status === 200) {
                context.commit("setUserAuthenticated", true);
            }
        }).catch((response) => {
            console.log(response);
            context.commit("setUserAuthenticated", false);
        });
    },
};

export default {
    state,
    getters,
    mutations,
    actions
};
