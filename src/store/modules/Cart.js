// import Vue from 'vue';

const state = {
    orders: [],
    address: [],
    mainAddress: {},
};

const getters = {
    orders(state) {
      return state.orders;
    },
    address(state) {
        return state.address;
    },
    mainAddress(state) {
        return state.mainAddress;
    },
    itemCount: state => state.orders.reduce((total, order) => total + order.quentity, 0),
    // totalPrice: state => state.orders.reduce((total, order) => total + (order.quentity * order.product.price), 0),
    totalPrice: state => state.orders.reduce((total, order) => (order.product.discount) ? (total + (order.quentity * order.product.discount)) : (total + (order.quentity * order.product.price)), 0),
    isExistCart: state => state.orders.length !== 0
};

const mutations = {
    addProduct(state, product) {
        let order = state.orders.find(order => order.product.id === product.id);
        if (order != null) {
            order.quantity++;
        } else {
            state.orders.push({product: product, quantity: 1});
        }
    },
    removeProduct(state, orderRemove) {
        let index = state.orders.findIndex(order => order === orderRemove);
        if (index > -1) {
            state.orders.splice(index, 1);
        }
    },
    setCartOrders(state, orders) {
      state.orders = orders;
    },
    setAddress(state, address) {
        state.address = address;
    },
    setMainAddress(state, mainAddress) {
        state.mainAddress = mainAddress;
    },
};

const actions = {
    getCartOrdersAction(context) {
      let orders = [
        {id: 1, quentity: 2, product: {id:1 , title: "عسل درجه یک اشترانکوه", description: "بدون موم", unit: "کیلوگرم", weight: 1, price: 200000, discount: null, imagelink: "honey-larg.png"}},
        {id: 2, quentity: 1, product: {id:2 , title: "عسل درجه دو اشترانکوه", description: "موم دار", unit: "کیلوگرم", weight: 1, price: 80000, discount: 50000, imagelink: "honey-larg.png"}}
      ];
      context.commit("setCartOrders", orders);
    },
    addProductToOrderAction(context, product) {
        console.log(product.id);
        context.commit("addProduct", product);
    },
    deleteProductFromOrderAction(context, product) {
        console.log(product.id);
        context.commit("removeProduct", product);
    },
    getAddressAction(context) {
        let address = [
            {id: 1, state: {id:1 , name: "تهران"}, city: {id:1 , name: "تهران"}, fullAddress: "خیابان حافظ شمالی جنب داروخانه مصدق"},
            {id: 2, state: {id:1 , name: "تهران"}, city: {id:2 , name: "تهران"}, fullAddress: "بزرگراه نواب خیابان مرتضوی کوچه کیوان"}
        ];
        context.commit("setAddress", address);
    },
    setMainAddressAction(context, mainAddress) {
        context.commit("setMainAddress", mainAddress);
    },
    completeShoppingAction() {

    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
