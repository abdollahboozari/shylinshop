import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
// import Login from "../views/Login";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        // component: Home
        component: () => import(/* webpackChunkName: "home" */ '../views/Home')
    },
    {
        path: '/signup',
        name: 'Signup',
        component: () => import(/* webpackChunkName: "signup" */ '../views/auth/Signup')
    },
    {
        path: '/activate',
        name: 'Activate',
        component: () => import(/* webpackChunkName: "activate" */ '../views/auth/Activation')
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "login" */ '../views/auth/Login')
    },
    {
        path: '/forgotpassword',
        name: 'Forgotpassword',
        component: () => import(/* webpackChunkName: "forgotpassword" */ '../views/auth/ForgotPassword')
    },
    {
        path: '/product',
        name: 'Product',
        component: () => import(/* webpackChunkName: "login" */ '../views/product/Product')
    },
    {
        path: '/productdetail',
        name: 'ProductDetail',
        component: () => import(/* webpackChunkName: "productdetail" */ '../views/product/ProductDetail')
    },
    {
        path: '/order',
        name: 'Order',
        component: () => import(/* webpackChunkName: "order" */ '../views/order/Order')
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '*',
        name:'404',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home')
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router
